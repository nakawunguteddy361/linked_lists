#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int num;
    struct Node *next;
};

// Function to create a new Node
struct Node *createNode(int num) {
    struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Not enough memory available\n");
        exit(1);
    }

    newNode->num = num;
    newNode->next = NULL;
    return newNode;
}

//Function to print linked lists
void printList(struct Node *head) {
    struct Node *list = head;
    if (list == NULL) {
    printf("List is empty.\n");
    return;
    } 
    printf("List: ");
    while (list != NULL) {
    printf("%d ->", list->num);
    list = list->next;
    }
}

//Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
    return;
    }
    struct Node *current = *head;
    while (current->next != NULL){
        current = current->next;
    }
    current->next = newNode;
    
}

//Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

//Function to delete a node by key
void deleteByKey(struct Node **head, int key){
    if (*head == NULL) {
        printf("List is empty. Deletion failed.\n");
        return;
    }
    struct Node *list = *head;
    struct Node *prev = NULL;
    if (list != NULL && list->num != key){
        prev = list;
        list = list->next;
    }
    if (list == NULL) {
        printf("Key not found. Deletion failed.\n");
        return;
    }
    prev->next = list->next;
    free(list);
}

//Function to delete a node by value
void deleteByValue(struct Node **head, int value) {
    if (*head == NULL) {
        printf("List is empty. Deletion failed.\n");
        return;
    } 
    struct Node *list = *head;
    struct Node *prev = NULL;
    if (list != NULL && list->num != value) {
        *head = list->next;
        free(list);
        return;
    }
    while (list != NULL && list->num != value) {
        prev = list;
        list = list->next;
    }
    if (list == NULL) {
        printf("Value not found. Deletion failed.\n");
        return;
    }
    prev->next = list->next;
    free(list);
}

//Function to insert a node after a specified key
void insertAfterKey(struct Node **head, int key, int value) {
    struct Node *list = *head;
    while (list != NULL && list->num != key) {
        list = list->next;
    }
    if (list == NULL) {
        printf("Key not found. Insertion failed.\n");
        return;
    }
    struct Node *newNode = createNode(value);
    newNode->next = list->next;
    list->next = newNode;
}

//Function to insert a node after a specified value
void insertAfterValue(struct Node **head, int searchValue, int newValue) {
    struct Node *list = *head;
    while (list != NULL && list->num != searchValue){
        list = list->next;
    }
    if (list == NULL){
        printf("Value not found. Insertion failed.\n");
        return;
    }
    struct Node *newNode = createNode(newValue);
    newNode->next = list->next;
    list->next = newNode;
}

int main()
{
    struct Node *head = NULL;
    int choice, data, key, value;

    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by key\n");
        printf("5. Delete by value\n");
        printf("6. Insert after key\n");
        printf("7. Insert after value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice)
        
        {
        case 1:    
            printList(head);
            break;
    
        case 2:
            printf("Enter data to append");
            scanf("%d", &data);
            append(&head, data);
            break;

        case 3:    
            printf("Enter data to prepend");
            scanf("%d", &data);
            prepend(&head, data);
            break;

        case 4:
            printf("Enter key to delete");
            scanf("%d", &key);
            deleteByKey(&head, key);
            break;

        case 5:
            printf("Enter value to delete");
            scanf("%d", &value);
            deleteByValue(&head, value);
            break;

        case 6:
            printf("Enter key after which to insert");
            scanf("%d", &key);
            printf("Enter value to insert");
            scanf("%d", &value);
            insertAfterKey(&head, key, value);
            break;

        case 7:
            printf("Enter value after which to insert");
            scanf("%d", &value);
            printf("Enter new value to insert");
            scanf("%d", &data);
            insertAfterValue(&head, value, data);
            break;

        case 8:
            printf("Exiting\n");
            exit(0);

            default:
            printf("Invalid choice. Please try again.\n");

        }
    }

    return 0;
}
